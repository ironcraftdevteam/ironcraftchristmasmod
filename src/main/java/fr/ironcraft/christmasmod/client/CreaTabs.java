package fr.ironcraft.christmasmod.client;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;

/**
 * @author Dermenslof
 */
public class CreaTabs extends CreativeTabs
{
	public static final CreaTabs ChristmasTab = new CreaTabs(CreativeTabs.getNextID(), "ChristmasTab", Item.getItemFromBlock(Blocks.bedrock));

	private Item itemIcon;

	public CreaTabs(int par1, String par2Str, Item item)
	{
		super(par1, par2Str);
		this.itemIcon = item;
	}

	@Override
	public Item getTabIconItem()
	{
		return this.itemIcon;
	}
	
	public void setTabIconItem(Item item)
	{
		this.itemIcon = item;
	}
}
