package fr.ironcraft.christmasmod.client;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import fr.ironcraft.christmasmod.common.CommonProxy;
import fr.ironcraft.christmasmod.common.init.ICBlocks;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;

/**
 * @author Dermenslof
 */
public class ClientProxy extends CommonProxy
{
	//public static int renderXXXID, renderYYYID;
	
	@Override
	public void init()
	{
		events();
		renders();
	}
	
	public void renders()
	{
//		renderXXXID = RenderingRegistry.getNextAvailableRenderId();
//		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ICBlocks.XXX), new RenderItemXXX());
//		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityXXX.class, new RenderTileEntityXXX());
//		renderYYYID = RenderingRegistry.getNextAvailableRenderId();
//		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ICBlocks.YYY), new RenderItemYYY());
//		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityYYY.class, new RenderTileEntityYYY());
	}
	
	public void events()
	{
		MinecraftForge.EVENT_BUS.register(new EventHandler());
		FMLCommonHandler.instance().bus().register(new KeyHandler());
	}
}