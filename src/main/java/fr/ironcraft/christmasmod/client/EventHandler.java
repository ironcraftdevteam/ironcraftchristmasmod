package fr.ironcraft.christmasmod.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

/**
 * @author Dermenslof
 */
public class EventHandler
{
	@SubscribeEvent
	public void onPlayerJoinWorld(EntityJoinWorldEvent event)
	{
		Minecraft mc = FMLClientHandler.instance().getClient();
		Entity e = event.entity;
		if (e instanceof EntityClientPlayerMP)
		{
			EntityClientPlayerMP player = (EntityClientPlayerMP)e;
			if (player == mc.thePlayer)
				;//...
		}
	}
	
	@SubscribeEvent
	public void onPlayerInteract(PlayerInteractEvent event)
	{
		Minecraft mc = FMLClientHandler.instance().getClient();
		EntityPlayer e = event.entityPlayer;

		//...
	}
}