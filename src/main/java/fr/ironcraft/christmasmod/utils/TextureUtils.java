package fr.ironcraft.christmasmod.utils;

import fr.ironcraft.christmasmod.ChristmasMod;

/**
 * @authors Dermenslof, DrenBx
 */
public class TextureUtils
{
	public static String getTextureNameForBlocks(String name)
	{
		return ChristmasMod.MODID + ":" + name.replace("tile.", "");
	}

	public static String getTextureNameForBlocks(String name, String type)
	{
		return ChristmasMod.MODID + ":" + name.replace("tile.", "") + "_" + type;
	}

	public static String getTextureNameForItems(String name)
	{
		return ChristmasMod.MODID + ":" + name;
	}
	
	public static String getTextureNameForGui(String name)
	{
		return ChristmasMod.MODID + ":textures/gui/" + name + ".png";
	}
}
