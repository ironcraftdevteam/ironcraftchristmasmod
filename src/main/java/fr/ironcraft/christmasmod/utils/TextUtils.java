package fr.ironcraft.christmasmod.utils;

import net.minecraft.client.resources.I18n;

/**
 * @authors Dermenslof
 */
public class TextUtils
{
	public static String translate(String str)
	{
		return I18n.format(str, new Object[0]);
	}
}