package fr.ironcraft.christmasmod;


import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import fr.ironcraft.christmasmod.common.CommonProxy;
import fr.ironcraft.christmasmod.common.blocks.BlockPresent;
import fr.ironcraft.christmasmod.common.init.ICBlocks;
import fr.ironcraft.christmasmod.common.packet.PacketPipeline;
import fr.ironcraft.christmasmod.common.tileentities.TileEntityPresent;
import fr.ironcraft.christmasmod.items.ItemPresentPaper;
import fr.ironcraft.christmasmod.items.render.PresentPaperRenderer;

@Mod(modid = ChristmasMod.MODID, version = ChristmasMod.VERSION)
public class ChristmasMod
{
	/**
	 * Id (string) du mod.
	 */
	public static final String MODID = "christmasmod";

	/**
	 * Version du mod
	 */
	public static final String VERSION = "Alpha-0.1";

	// --- Début des blocks ---

	public static Block blockPresent;

	// --- Fin des blocks ---

	// =====

	// --- Début des items ---

	public static Item presentPaper;

	// --- Fin des items ---

	// =====

	 
	@Instance(MODID)
	public static ChristmasMod instance;

	@SidedProxy(clientSide = "fr.ironcraft."+MODID+".client.ClientProxy", serverSide = "fr.ironcraft."+MODID+".common.CommonProxy")
	public static CommonProxy proxy;
	public static Configuration config;
	private static ICBlocks blocks;
	public static final PacketPipeline pipeline = new PacketPipeline();
	
	// --- Début des biomes ---


	// --- Fin des biomes ---

	public static CreativeTabs christmasTab = new CreativeTabs("christmas") {
		@SideOnly(Side.CLIENT)
		public Item getTabIconItem()
		{
			return Item.getItemFromBlock(blockPresent);
		}
	};

	@EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
    	config = new Configuration(event.getSuggestedConfigurationFile());
		config.load();
		//...
		if (config.hasChanged())
			config.save();
    	blocks = new ICBlocks();
    	blocks.init();
    }
	
	/**
	 * Appelé lors de l'initialisation du jeu.
	 * @param event L'objet représentant l'event de démarrage du jeu.
	 */
	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		GameRegistry.registerTileEntity(TileEntityPresent.class, "Present");

		// Initialisation des items
		presentPaper = new ItemPresentPaper().setTextureName(MODID + ":present_paper").setUnlocalizedName("presentPaper").setCreativeTab(christmasTab);
		
		// Enregistrement des items
		GameRegistry.registerItem(presentPaper, "presentPaper");
		
		// Enregistrement des renderers des items
		MinecraftForgeClient.registerItemRenderer(presentPaper, new PresentPaperRenderer());
		
		// Initialisation des blocks
		blockPresent = new BlockPresent().setBlockName("blockPresent").setBlockTextureName(MODID + ":present_side").setCreativeTab(christmasTab);

		// Enregistrement des blocks dans le GameRegistry
		GameRegistry.registerBlock(blockPresent, "blockPresent");
		proxy.init();
		pipeline.initialise();
	}
	
	@EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
    	pipeline.postInitialise();
    	System.out.println("[IC ChrisMasMod] Version: "+VERSION+" was loaded");
    }
}
