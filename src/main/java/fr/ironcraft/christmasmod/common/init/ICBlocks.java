package fr.ironcraft.christmasmod.common.init;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import cpw.mods.fml.common.registry.GameRegistry;
import fr.ironcraft.christmasmod.client.CreaTabs;
import fr.ironcraft.christmasmod.common.blocks.BlockPresent;
import fr.ironcraft.christmasmod.common.tileentities.TileEntityPresent;
import fr.ironcraft.christmasmod.utils.TextureUtils;

/**
 * @authors Dermenslof, DrenBx
 */
public class ICBlocks
{
	public static Block present;
	
	public void init()
	{
		this.configBlocks();
		this.registerBlocks();
	}

	private void registerBlocks()
	{
		GameRegistry.registerBlock(present, "present");
		GameRegistry.registerTileEntity(TileEntityPresent.class, "Present");
		
		CreaTabs.ChristmasTab.setTabIconItem(Item.getItemFromBlock(present));
	}

	private void configBlocks()
	{
		present = new BlockPresent().setBlockName("present").setBlockTextureName(TextureUtils.getTextureNameForBlocks("present_side")).setCreativeTab(CreaTabs.ChristmasTab);
	}
}
