package fr.ironcraft.christmasmod.common.tileentities;


import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import fr.ironcraft.christmasmod.common.blocks.BlockPresent;
import fr.ironcraft.christmasmod.utils.Color;
import fr.ironcraft.christmasmod.utils.fireworks.FireworkEffect;
import fr.ironcraft.christmasmod.utils.fireworks.FireworkEffect.Type;


public class TileEntityPresent extends TileEntity
{
	/**
	 * L'item cadeau en question. Il sera donné (pouvant être récupéré dans un
	 * guicontainer) au clic sur le block.
	 */
	private ItemStack thePresent;

	/**
	 * Crée une nouvelle TileEntity pour les cadeaux.
	 * @param present L'item cadeau qui sera offet à la personne qui ouvrira.
	 */
	public TileEntityPresent(ItemStack present)
	{
		this.thePresent = present;
	}

	public TileEntityPresent()
	{
		this(new ItemStack(Blocks.stone));
	}

	public ItemStack getPresent()
	{
		return thePresent;
	}

	public void setPresent(ItemStack thePresent)
	{
		this.thePresent = thePresent;
	}

	/**
	 * Appelé à l'enregistrement de l'entité dans les données du monde.
	 */
	public void writeToNBT(NBTTagCompound par1NBTTagCompound)
	{
		super.writeToNBT(par1NBTTagCompound);

		// On sauve l'item cadeau
		this.thePresent.writeToNBT(par1NBTTagCompound);
	}

	/**
	 * Appelé à la lecture de l'entité depuis les données du monde.
	 */
	public void readFromToNBT(NBTTagCompound par1NBTTagCompound)
	{
		super.readFromNBT(par1NBTTagCompound);

		// On récupère l'item cadeau
		this.thePresent = ItemStack.loadItemStackFromNBT(par1NBTTagCompound);
	}


	/**
	 * Appelé lors du clic droit sur le bloc (délégué depuis {@link BlockPresent}).
	 * @param par1EntityPlayer
	 * @param par2
	 * @param par3
	 * @param par4
	 * @param par5
	 */
	public void handleRightClick(EntityPlayer par1EntityPlayer, int par2, float par3, float par4, float par5)
	{
		FireworkEffect effect = FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.WHITE).withFade(Color.GRAY).build();

		this.worldObj.makeFireworks(this.xCoord + 0.5D, this.yCoord + 0.5D, this.zCoord + 0.5D, 0.0D, 0.0D, 0.0D, FireworkEffect.makeReadableNBT(effect.toExplosionTag()));

		// TODO : Animation d'ouverture du cadeau
		// TODO : et faire apparaître l'item au milieu
	}
}