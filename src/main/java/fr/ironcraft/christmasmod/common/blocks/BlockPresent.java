package fr.ironcraft.christmasmod.common.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import fr.ironcraft.christmasmod.ChristmasMod;
import fr.ironcraft.christmasmod.common.tileentities.TileEntityPresent;

public class BlockPresent extends BlockContainer
{
	private IIcon topIcon;
	private IIcon bottomIcon;

	public BlockPresent()
	{
		super(Material.glass);
	}

	/**
	 * Appelé quand le bloc est placé par une entité (par5).
	 */
	public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLiving, ItemStack par6ItemStack)
	{
		// TODO : set l'item dans la tileEntity

		super.onBlockPlacedBy(par1World, par2, par3, par4, par5EntityLiving, par6ItemStack);

		TileEntityPresent tileEntity = (TileEntityPresent) par1World.getTileEntity(par2, par3, par4);

		// -> tileEntity.setPresent(unTrucQuonChoppeDansLeNBTDuPar6);
	}

	@Override
	public TileEntity createNewTileEntity(World par1World, int par2)
	{
		return new TileEntityPresent();
	}

	/**
	 * Appelé lors du clic droit sur le bloc. Délègue l'action à la TileEntity.
	 */
	public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
	{
		TileEntityPresent tileEntity = (TileEntityPresent) par1World.getTileEntity(par2, par3, par4);
		tileEntity.handleRightClick(par5EntityPlayer, par6, par7, par8, par9);
		return super.onBlockActivated(par1World, par2, par3, par4, par5EntityPlayer, par6, par7, par8, par9);
	}

	/**
	 * Appelé à l'enregistrement du block : sert à trouver et enregistrer les textures.
	 */
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
		super.registerBlockIcons(par1IconRegister);
		this.topIcon = par1IconRegister.registerIcon(ChristmasMod.MODID + ":present_top");
		this.bottomIcon = par1IconRegister.registerIcon(ChristmasMod.MODID + ":present_bot");
	}

	/**
	 * Appelé pour trouver l'icône du block suivant la face et la meta.
	 */
	public IIcon getIcon(int side, int meta)
	{
		return side == 0 ? this.bottomIcon : (side == 1 ? this.topIcon : this.blockIcon);
	}

}
