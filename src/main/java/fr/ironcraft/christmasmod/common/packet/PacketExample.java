package fr.ironcraft.christmasmod.common.packet;

import fr.ironcraft.christmasmod.ChristmasMod;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

/**
 * @author Dermenslof
 */
public class PacketExample implements IBasePacket
{
	private String data;
	
	public PacketExample()
	{
		
	}

	public PacketExample(String d)
	{
		this.data = d;
	}
	
	@Override
	public void encodeInto(ChannelHandlerContext ctx, PacketBuffer buffer)
	{
    	try
    	{
    		buffer.writeStringToBuffer(this.data);
		}
    	catch (Exception e)
    	{
			e.printStackTrace();
		}
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, PacketBuffer buffer)
	{
		try
		{
			this.data = buffer.readStringFromBuffer(buffer.capacity());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void handleClientSide(EntityPlayer player)
	{
		System.out.println("reception packet example");
		//...
	}

	@Override
	public void handleServerSide(EntityPlayer player)
	{
		//...
		ChristmasMod.pipeline.sendToAll(this);
	}
}
